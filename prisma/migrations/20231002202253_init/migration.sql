/*
  Warnings:

  - You are about to drop the column `number` on the `Pizza` table. All the data in the column will be lost.
  - Added the required column `cost` to the `Pizza` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Pizza" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "title" TEXT NOT NULL,
    "cost" INTEGER NOT NULL,
    "img" TEXT NOT NULL
);
INSERT INTO "new_Pizza" ("id", "img", "title") SELECT "id", "img", "title" FROM "Pizza";
DROP TABLE "Pizza";
ALTER TABLE "new_Pizza" RENAME TO "Pizza";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
