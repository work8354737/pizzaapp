"use client";

import BasketPizza from "@/components/BasketPizza/BasketPizza";
import { basketState } from "@/store/BasketState";
import { pizzaState } from "@/store/PizzaState";
import { observer } from "mobx-react-lite";
import { useRouter } from "next/navigation";
import React, { useEffect } from "react";

const BasketPage: React.FC = () => {
  const { push } = useRouter();
  useEffect(() => {
    if (!basketState.items.length) {
      push("/");
    }
  }, []);

  const uniqueItems = Array.from(new Set(basketState.items));

  const basketPizzas = uniqueItems.map((pizzaId) => {
    const currentPizza = pizzaState.pizzas.find((p) => p.id === pizzaId);

    return {
      ...currentPizza,
      amount: basketState.items.filter((id) => id === pizzaId).length,
    };
  });

  return (
    <>
      <section className="section">
        <div className="container">
          <div className="basket-setcion">
            {basketPizzas.map((p) => (
              <BasketPizza key={p.id} {...p} />
            ))}
          </div>
        </div>
      </section>
    </>
  );
};

export default observer(BasketPage);
