"use client";
import { PizzaService } from "@/actions/PizzaService";
import { useControl } from "@/hooks/useControl";
import { pizzaState } from "@/store/PizzaState";
import { useRouter } from "next/navigation";

const readFileThroughFileReader = (file: File) => {
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    reader.onload = (event) => {
      resolve(event.target?.result);
    };

    reader.readAsDataURL(file);
  });
};

import React, { useState } from "react";

const New = () => {
  const [title_model, title] = useControl();
  const [cost_model, cost] = useControl();
  const [file, setFile] = useState<File>();

  const { push } = useRouter();

  return (
    <>
      <div className="container">
        <form
          className="add-form"
          onSubmit={async (event) => {
            event.preventDefault();
            if (!file) return;
            pizzaState.addPizza({
              title,
              cost: Number(cost),
              img: (await readFileThroughFileReader(file)) as string,
            });
            push("/");
          }}
        >
          <h1 className="title">Добавить пиццу</h1>
          <input
            className="field"
            type="text"
            placeholder="Название"
            {...title_model}
          />
          <input
            className="field"
            type="number"
            placeholder="Цена"
            {...cost_model}
          />
          <input
            type="file"
            accept="image/*"
            onChange={(event) => {
              setFile(event.currentTarget.files?.[0]);
            }}
          />
          <button className="btn">Добавить</button>
        </form>
      </div>
    </>
  );
};

export default New;
