import { prisma } from "@/actions/context";
import BasketProvider from "@/components/PizzaList/BasketProvider";
import PizzaList from "@/components/PizzaList/PizzaList";
import Search from "@/components/Search/Search";

export default async function Home() {
  const pizzas = await prisma.pizza.findMany();
  return (
    <>
      <main className="main">
        <Search />
        <BasketProvider>
          <PizzaList pizzas={pizzas} />
        </BasketProvider>
      </main>
    </>
  );
}
