import Header from "@/components/Header/Header";
import "./global.css";
import type { Metadata } from "next";
import { Roboto, Inter, Montserrat, Open_Sans } from "next/font/google";

const roboto = Open_Sans({
  subsets: ["cyrillic"],
  weight: ["400", "500", "600", "700"],
});

export const metadata: Metadata = {
  title: "Pizza",
  description: "Pizza",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={roboto.className}>
        <Header />
        {children}
      </body>
    </html>
  );
}
