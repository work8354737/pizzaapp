import React from "react";

const PizzaListLayout: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  return (
    <>
      <section className="section">
        <div className="container">
          <div className="grid">{children}</div>
        </div>
      </section>
    </>
  );
};

export default PizzaListLayout;
