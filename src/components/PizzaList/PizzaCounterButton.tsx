import React from "react";

interface PizzaCounterButtonProps {
  count: number;
  increament: () => void;
  decreament: () => void;
}

const PizzaCounterButton: React.FC<PizzaCounterButtonProps> = ({
  count,
  increament,
  decreament,
}) => {
  return (
    <>
      {!!count ? (
        <div className="counter">
          <button className="count-btn" onClick={decreament}>
            -
          </button>
          <span className="count-text">{count}</span>
          <button className="count-btn" onClick={increament}>
            +
          </button>
        </div>
      ) : (
        <button className="card-btn" onClick={increament}>
          Выбрать
        </button>
      )}
    </>
  );
};

export default React.memo(PizzaCounterButton);
