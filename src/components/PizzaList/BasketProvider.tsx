"use client";
import React, { useEffect } from "react";
import { basketState } from "@/store/BasketState";

interface BasketProviderProps {
  children: React.ReactNode;
}

const BasketProvider: React.FC<BasketProviderProps> = ({ children }) => {
  useEffect(() => {
    const jsonItems = localStorage.getItem("basket");
    if (!!jsonItems) basketState.setItems(JSON.parse(jsonItems));
  }, []);
  return <>{children}</>;
};

export default BasketProvider;
