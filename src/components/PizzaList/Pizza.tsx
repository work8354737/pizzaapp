"use client";
import { basketState } from "@/store/BasketState";
import { observer } from "mobx-react-lite";
import Image from "next/image";
import React, { useCallback } from "react";
import PizzaCounterButton from "./PizzaCounterButton";
import { IPizza } from "./PizzaList";

interface PizzaProps extends IPizza {}

const Pizza: React.FC<PizzaProps> = ({ id, title, img, cost }) => {
  const itemCount = basketState.items.filter((item) => item === id).length;
  const increamentCurrentItem = useCallback(
    () => basketState.addItem(id),
    [id]
  );
  const decreamentCurrentItem = useCallback(
    () => basketState.deleteItem(id),
    [id]
  );
  return (
    <div className="card">
      <Image
        className="card__img"
        src={img}
        width={150}
        height={150}
        alt={title}
      />

      <h3 className="card-title">{title}</h3>
      <div className="between-row">
        <span className="card-cost">{cost} ₽</span>

        <PizzaCounterButton
          increament={increamentCurrentItem}
          decreament={decreamentCurrentItem}
          count={itemCount}
        />
      </div>
    </div>
  );
};

export default observer(Pizza);
