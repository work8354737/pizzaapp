"use client";
import React, { useDeferredValue, useEffect, useState } from "react";
import PizzaListLayout from "./PizzaListLayout";
import Pizza from "./Pizza";
import BasketProvider from "./BasketProvider";
import { observer } from "mobx-react-lite";
import { searchState } from "@/store/SearchState";
import { pizzaState } from "@/store/PizzaState";

export interface IPizza {
  id: number;
  title: string;
  img: string;
  cost: number;
}

interface PizzaListPorps {
  pizzas: IPizza[];
}

const PizzaList: React.FC<PizzaListPorps> = () => {
  const pizzas = pizzaState.pizzas;

  const filteredItems = useDeferredValue(
    pizzas?.filter((item) => item.title.includes(searchState.search))
  );

  return (
    <>
      <PizzaListLayout>
        {filteredItems?.map((pizza) => (
          <Pizza key={pizza.id} {...pizza} />
        ))}
      </PizzaListLayout>
    </>
  );
};

export default observer(PizzaList);
