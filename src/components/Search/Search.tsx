"use client";

import { searchState } from "@/store/SearchState";
import { observer } from "mobx-react-lite";
import React from "react";

const Search: React.FC = () => {
  return (
    <>
      <div className="container search-container">
        <input
          value={searchState.search}
          onChange={(event) => searchState.setSearch(event.currentTarget.value)}
          type="text"
          className="field"
          placeholder="Поиск..."
        />
      </div>
    </>
  );
};

export default observer(Search);
