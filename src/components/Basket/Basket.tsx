"use client";
import { basketState } from "@/store/BasketState";
import { observer } from "mobx-react-lite";
import Link from "next/link";
import React from "react";

const Basket: React.FC = () => {
  return (
    <>
      {!!basketState.items.length && (
        <Link href={"/basket/"} className="basket">
          <img src="/basket.svg" alt="" />
        </Link>
      )}
    </>
  );
};

export default observer(Basket);
