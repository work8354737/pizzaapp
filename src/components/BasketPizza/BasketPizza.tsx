import React from "react";

interface BasketPizzaProps {
  img?: string;
  title?: string;
  amount?: number;
}

const BasketPizza: React.FC<BasketPizzaProps> = ({ img, title, amount }) => {
  return (
    <div className="basket-item">
      <img src={img} className="basket-item__img" />

      <div className="basket-item__content">
        <h3 className="card-title">{title}</h3>
        <p>Кол-во: {amount}</p>
      </div>
    </div>
  );
};

export default BasketPizza;
