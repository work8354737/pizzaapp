import Image from "next/image";
import Link from "next/link";
import React from "react";
import Basket from "../Basket/Basket";

const Header: React.FC = () => {
  return (
    <>
      <header className="header">
        <div className="container">
          <Image src={"/img/logo.png"} width={50} height={50} alt="Логотип" />
          <Link href="/" className="header-link">
            Пиццы
          </Link>
          <Link href="/new" className="header-link">
            Добавить новую
          </Link>
        </div>
        <Basket />
      </header>
    </>
  );
};

export default Header;
