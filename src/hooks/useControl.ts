import { useState } from "react";

export const useControl = (defaultValue = "") => {
  const [value, setValue] = useState(defaultValue);
  const onChange = (event: React.FormEvent<HTMLInputElement>) => {
    setValue(event.currentTarget?.value);
  };
  return [{ value, onChange }, value, setValue] as const;
};
