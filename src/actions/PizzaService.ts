"use server";
import { promises as fileSystem } from "fs";
import { prisma } from "./context";

interface PizzaI extends PizzaCreateI {
  id: number;
}

interface PizzaCreateI {
  title: string;
  img: File;
  cost: number;
}

const readFileThroughFileReader = (file: File) => {
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    reader.onload = (event) => {
      resolve(event.target?.result);
    };

    reader.readAsBinaryString(file);
  });
};

export const PizzaService = {
  async addPizza({ title, img, cost }: PizzaCreateI) {
    const fileContent = (await readFileThroughFileReader(img)) as string;
    const fileName = img.name;

    await fileSystem.writeFile(`../../public/media/${fileName}`, fileContent);
    prisma.pizza.create({
      data: { title, cost, img: `/media/${fileName}` },
    });
  },
};
