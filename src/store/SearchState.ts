import { makeAutoObservable } from "mobx";

class SearchState {
  search: string = "";
  constructor() {
    makeAutoObservable(this);
  }
  setSearch(value: string) {
    this.search = value;
  }
}

export const searchState = new SearchState();
