import { makeAutoObservable } from "mobx";

class BasketState {
  items: number[] = [];

  constructor() {
    makeAutoObservable(this);
  }

  addItem(id: number) {
    this.items.push(id);
    this.updateStorageBasket();
  }

  deleteItem(id: number) {
    const index = this.items.indexOf(id);
    if (index !== -1) {
      this.items.splice(index, 1);
      this.updateStorageBasket();
    }
  }

  setItems(items: number[]) {
    this.items = items;
  }

  private updateStorageBasket() {
    localStorage.setItem("basket", JSON.stringify(this.items));
  }
}

export const basketState = new BasketState();
