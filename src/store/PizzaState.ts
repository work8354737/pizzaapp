import { IPizza } from "@/components/PizzaList/PizzaList";

class PizzaState {
  pizzas: IPizza[] = [];

  constructor() {
    const pizzasJson = localStorage.getItem("pizzas");
    if (!pizzasJson) return;
    try {
      this.pizzas = JSON.parse(pizzasJson) as IPizza[];
    } catch (_) {}
  }

  addPizza(pizza: Omit<IPizza, "id">) {
    const newPizza = { id: new Date().getTime(), ...pizza };
    const pizzasJson = localStorage.getItem("pizzas");

    this.pizzas.unshift(newPizza);

    if (!pizzasJson) {
      localStorage.setItem("pizzas", JSON.stringify([newPizza]));
      return;
    }
    try {
      const savedPizzas = JSON.parse(pizzasJson) as IPizza[];
      localStorage.setItem(
        "pizzas",
        JSON.stringify([...savedPizzas, newPizza])
      );
    } catch (_) {}
  }
}

export const pizzaState = new PizzaState();

// [
//   {
//     id: 1,
//     title: "Пеперони",
//     img: "/img/pizza.png",
//     cost: 600,
//   },
// ];
