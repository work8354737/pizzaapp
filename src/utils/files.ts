const readFileThroughFileReader = (file: File) => {
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    reader.onload = (event) => {
      resolve(event.target?.result);
    };

    reader.readAsDataURL(file);
  });
};
